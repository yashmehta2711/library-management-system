-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2020 at 02:01 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookissue`
--

CREATE TABLE `bookissue` (
  `student_id` varchar(255) NOT NULL,
  `book_id` varchar(255) NOT NULL,
  `date_of_issue` date NOT NULL,
  `date_of_return` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookissue`
--

INSERT INTO `bookissue` (`student_id`, `book_id`, `date_of_issue`, `date_of_return`) VALUES
('CEBE01', 'CS1', '2020-05-01', '2020-05-06'),
('CSFY02', 'H1', '2020-05-06', '2020-05-13'),
('CSFY02', 'CS1', '2020-05-01', '2020-05-06'),
('CSFY03', 'H1', '2020-05-04', '2020-05-07');

-- --------------------------------------------------------

--
-- Table structure for table `bookrequest`
--

CREATE TABLE `bookrequest` (
  `book_name` varchar(255) NOT NULL,
  `publication_name` varchar(255) NOT NULL,
  `quantity` int(10) NOT NULL,
  `publication_number` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookrequest`
--

INSERT INTO `bookrequest` (`book_name`, `publication_name`, `quantity`, `publication_number`) VALUES
('Java', 'HT', 5, 8879090961),
('The Rule Of Law', 'Nova Science', 5, 9819869400),
('The Rule Of Law', 'Navneet', 5, 9820254233),
('Rich dad poor dad', 'Nova Science', 5, 9819869400),
('The Rule Of Law', 'SAGE', 8, 9820293336),
('Rich dad poor dad', 'Nova Science', 5, 9819869400),
('The Rule Of Law', 'Nova Science', 4, 9819869400),
('Rich dad poor dad', 'Nova Science', 2, 9819869400),
('Adolf Hitler', 'SAGE', 5, 9820293336),
('Rich dad poor dad', 'SAGE', 10, 9820293336),
('Rich dad poor dad', 'SAGE', 8, 9820293336),
('The Rule Of Law', 'SAGE', 2, 9820293336),
('Adolf Hitler', 'SAGE', 5, 9820293336),
('Adolf Hitler', 'SAGE', 6, 9820293336),
('Java', 'Navneet', 10, 9820254233),
('Java', 'Navneet', 10, 9820254233),
('Rich dad poor dad', 'Oxford University Press', 7, 7977951472),
('Rich dad poor dad', 'Vishal', 10, 7977950934),
('The Rule Of Law', 'Deepam', 9, 8369678513);

-- --------------------------------------------------------

--
-- Table structure for table `bookreturn`
--

CREATE TABLE `bookreturn` (
  `student_id` varchar(255) NOT NULL,
  `book_id` varchar(255) NOT NULL,
  `book_condition` varchar(255) NOT NULL,
  `fine` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookreturn`
--

INSERT INTO `bookreturn` (`student_id`, `book_id`, `book_condition`, `fine`) VALUES
('n7', 'H1', 'Proper', 0),
('CSFY02', 'F1', 'Imporoper', 100),
('CSFY02', 'F1', 'Imporoper', 100),
('CSFY02', 'F1', 'Imporoper', 100),
('CSFY02', 'F1', 'Imporoper', 100),
('n4', 'H1', 'Proper', 0),
('EESY01', 'H1', 'Proper', 0),
('METY01', 'F1', 'Proper', 0),
('CSSY01', 'CS1', 'Proper', NULL),
('CSSY01', 'CS1', 'Proper', NULL),
('METY01', 'F1', 'Proper', NULL),
('EESY01', 'H1', 'Imporoper', 30),
('CSFY02', 'F1', 'Imporoper', 50),
('CSFY02', 'F1', 'Imporoper', 50),
('CSFY02', 'F1', 'Imporoper', 50),
('CSSY01', 'CS1', 'Imporoper', 50),
('CSSY01', 'CS1', 'Imporoper', 50),
('n7', 'H1', 'Imporoper', 50),
('n4', 'H1', 'Imporoper', 50),
('CSFY02', 'CS1', 'Imporoper', 50),
('EESY01', 'H1', 'Imporoper', 60),
('CSSY01', 'CS1', 'Imporoper', 50),
('CSSY01', 'F1', 'Imporoper', 50),
('EESY01', 'F1', 'Proper', NULL),
('CSFY02', 'LA1', 'Improper', 100),
('EESY01', 'H1', 'Proper', NULL),
('CSFY02', 'CS1', 'Proper', NULL),
('CSFY03', 'LA1', 'Proper', NULL),
('n1', 'F1', 'Proper', NULL),
('CSFY01', 'F1', 'Proper', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` varchar(255) NOT NULL,
  `book_name` varchar(255) NOT NULL,
  `book_author` varchar(255) NOT NULL,
  `book_quantity` int(100) NOT NULL,
  `publication_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `book_name`, `book_author`, `book_quantity`, `publication_name`) VALUES
('CS1', 'Java', 'James Gosling', 6, 'HT'),
('F1', 'Rich dad poor dad', 'Robert', 14, 'Navneet'),
('H1', 'Adolf Hitler', 'Adolf', 9, 'Techmax'),
('LA1', 'The Rule Of Law', 'Tom Bingham', 6, 'Oxford University Press');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`) VALUES
('admin', 'admin'),
('root', 'root');

-- --------------------------------------------------------

--
-- Table structure for table `look_up_id`
--

CREATE TABLE `look_up_id` (
  `subject` varchar(255) NOT NULL,
  `subject_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `look_up_id`
--

INSERT INTO `look_up_id` (`subject`, `subject_code`) VALUES
('Accounts', 'A'),
('Computer Science', 'CS'),
('Finance', 'F'),
('Geography', 'G'),
('History', 'H'),
('Literature', 'L'),
('Law', 'LA'),
('Medical', 'M'),
('Marketting', 'MK'),
('Reference ', 'R'),
('Science', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `publication_id` varchar(255) NOT NULL,
  `publication_name` varchar(255) NOT NULL,
  `publication_number` varchar(255) NOT NULL,
  `publication_email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`publication_id`, `publication_name`, `publication_number`, `publication_email`) VALUES
('p4', 'BrownWalker Press', '9820251333', 'bwp@gmail.com'),
('p16', 'Deepam', '8369678513', 'deepam@gmail.com'),
('p7', 'HT', '8879090961', 'ht@gmail.com'),
('p1', 'Navneet', '9820254233', 'abc@gmail.com'),
('p2', 'Nova Science', '9819869400', 'xyz@gmail.com'),
('p5', 'Oxford University Press', '7977951472', 'oup@gmail.com'),
('p8', 'Prem', '9004425292', 'prem@gmail.com'),
('p3', 'SAGE', '9820293336', 'SAGE@gmail.com'),
('p6', 'Techmax', '9867225588', 'tm@gmail.com'),
('p15', 'Vishal', '7977950934', 'vishal@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` varchar(255) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `student_rno` int(10) DEFAULT NULL,
  `student_gender` varchar(255) NOT NULL,
  `student_department` varchar(255) NOT NULL,
  `student_year` int(10) DEFAULT NULL,
  `student_fine` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`student_id`, `student_name`, `student_rno`, `student_gender`, `student_department`, `student_year`, `student_fine`) VALUES
('CEBE01', 'Dipti', 1, 'Female', 'Civil', 4, 50),
('CSFY01', 'Darsh', 1, 'Female', 'Computer Science', 1, 100),
('CSFY02', 'Ashay', 4, 'Male', 'Computer Science', 1, 100),
('CSFY03', 'Bhavya', 6, 'Male', 'Computer Science', 1, 200),
('CSSY01', 'Yash', 1, 'Male', 'Computer Science', 2, 501),
('EESY01', 'Chintan', 1, 'Male', 'Electrical', 2, 20),
('METY01', 'Mehul', 1, 'Male', 'Mechanical', 3, 0),
('METY02', 'Nikhil', 4, 'Male', 'Mechanical', 3, 0),
('METY03', 'Shubham', 4, 'Male', 'Mechanical', 3, 60),
('n1', 'Raj', 0, 'Male', '', 0, 100),
('n10', 'Ravi', NULL, 'Male', 'Others', NULL, 20),
('n11', 'Dhairyaa', NULL, 'Female', 'Others', NULL, 0),
('n2', 'Naman', 0, 'Male', '', 0, 0),
('n3', 'Deepam', 0, 'Male', 'Others', 0, 0),
('n4', 'Sophiya', 0, 'Female', '', 0, 0),
('n7', 'Rupal', 0, 'Female', 'Others', 1, 0),
('n8', 'Shirish', 0, 'Male', 'Others', 0, 50),
('n9', 'abc', NULL, 'Male', 'Others', NULL, 600);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookissue`
--
ALTER TABLE `bookissue`
  ADD KEY `student_id` (`student_id`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `bookreturn`
--
ALTER TABLE `bookreturn`
  ADD KEY `student_id` (`student_id`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `publication_name` (`publication_name`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `look_up_id`
--
ALTER TABLE `look_up_id`
  ADD PRIMARY KEY (`subject_code`),
  ADD UNIQUE KEY `subject_code` (`subject_code`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`publication_name`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookissue`
--
ALTER TABLE `bookissue`
  ADD CONSTRAINT `bookissue_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `bookissue_ibfk_3` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`);

--
-- Constraints for table `bookreturn`
--
ALTER TABLE `bookreturn`
  ADD CONSTRAINT `bookreturn_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  ADD CONSTRAINT `bookreturn_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`);

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`publication_name`) REFERENCES `publication` (`publication_name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
