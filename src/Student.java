
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class Student extends javax.swing.JPanel {

    /**
     * Creates new form Student
     */
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Student student;
    
    public static final int NOP = 0;
    public static final int INSERT = 1;
    public static final int UPDATE = 2;
    int operation;
    String gender;
    public Student() {
        initComponents();
        this.student = this;
        conn = MysqlConnect.connectDB();
        String sql = "SELECT * FROM students";
        updateTableData(sql);
        disableAll();
        tSearch.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
    }
    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"ID", "Name", "Roll Number", "Gender", "Department", "Year", "Fine"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    private void enableAll(){
        tSID.setEnabled(true);
        tName.setEnabled(true);
        rbMale.setEnabled(true);
        rbFemale.setEnabled(true);
        tRollNo.setEnabled(true);
        cbDept.setEnabled(true);
        cbYear.setEnabled(true);
        tFine.setEnabled(true);
    }
    
    private void disableAll(){
       tSID.setEnabled(false);
        tName.setEnabled(false);
        rbMale.setEnabled(false);
        rbFemale.setEnabled(false);
        tRollNo.setEnabled(false);
        cbDept.setEnabled(false);
        cbYear.setEnabled(false);
        tFine.setEnabled(false);  
    }
    
    private void searchFilter(){
        String key = tSearch.getText();
        String sql;
        if(key.equals("")){
            sql = "SELECT * FROM students";
        }else{
            sql = "SELECT * FROM students WHERE student_name like '%"+key+"%' OR student_id like '%"+key+"%' OR student_rno like '%"+key+"%' OR student_gender like '%"+key+"%' OR student_department like '%"+key+"%' OR student_year like '%"+key+"%' OR student_fine like '%"+key+"%'";
        }
        updateTableData(sql);
    }
    
    private void insertRecord(){
        if(cbYear.getSelectedItem().toString()!= "0"){    
            String sql = "INSERT INTO students(student_id, student_name, student_rno, student_gender, student_department, student_year, student_fine) VALUES (?, ?, ?, ?, ?, ?, ?)";
            String id = tSID.getText();
            String name = tName.getText();
            String rno = tRollNo.getText();
            String department = cbDept.getSelectedItem().toString();
            String year = cbYear.getSelectedItem().toString();
            int fine = Integer.parseInt(tFine.getText());
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, id);
                ps.setString(2, name);
                ps.setString(3, rno);
                ps.setString(4, gender);
                ps.setString(5, department);
                ps.setString(6, year);
                ps.setInt(7, Integer.parseInt(tFine.getText()));

                ps.execute();
                clearFields();
                String loadTableQuery = "SELECT * FROM students";
                updateTableData(loadTableQuery);
                JOptionPane.showMessageDialog(this, "Record Inserted Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
            }
        }else{
            String sql = "INSERT INTO students(student_id, student_name, student_rno, student_gender, student_department, student_fine) VALUES (?, ?, ?, ?, ?, ?)";
            String id = tSID.getText();
            String name = tName.getText();
            String rno = null;
            String department = cbDept.getSelectedItem().toString();
            String year = cbYear.getSelectedItem().toString();
            int fine = Integer.parseInt(tFine.getText());
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, id);
                ps.setString(2, name);
                ps.setString(3, rno);
                ps.setString(4, gender);
                ps.setString(5, department);
                ps.setInt(6, fine);

                ps.execute();
                clearFields();
                String loadTableQuery = "SELECT * FROM students";
                updateTableData(loadTableQuery);
                JOptionPane.showMessageDialog(this, "Record Inserted Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
            }
        }
    }
    
    private void updateRecord(){
        
        if(!"".equals(tSID.getText())){
            String sql = "UPDATE students SET student_id = ?, student_name = ?, student_rno = ?, student_gender = ?, student_department = ?, student_year = ?, student_fine = ? WHERE student_id = ?";
            String id = tSID.getText();
            String name = tName.getText();
            String rno = tRollNo.getText();
            String department = cbDept.getSelectedItem().toString();
            String year = cbYear.getSelectedItem().toString();
            String fine = tFine.getText();
               
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, id);
                ps.setString(2, name);
                ps.setString(3, rno);
                ps.setString(4, gender);
                ps.setString(5, department);
                ps.setString(6, year);
                ps.setString(7, fine);
                ps.setString(8, id);

                ps.execute();
                clearFields();
                String loadTableQuery = "SELECT * FROM students";
                updateTableData(loadTableQuery);
                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
       }
    }
    
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            SDetails.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }finally{
            try{
                rs.close();
                ps.close();
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
            }
        }
    }
    
    private void clearFields(){
        tSID.setText("");
        tName.setText("");
        tRollNo.setText("");
        rbTemp.setSelected(true);
        rbMale.setSelected(false);
        rbFemale.setSelected(false);
        gender = null;
        cbDept.setSelectedIndex(-1);
        cbYear.setSelectedIndex(-1);
        tFine.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbTemp = new javax.swing.JRadioButton();
        pOnStudents = new javax.swing.JPanel();
        pDetails = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tRollNo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        tSID = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        tName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        rbMale = new javax.swing.JRadioButton();
        rbFemale = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cbDept = new javax.swing.JComboBox<>();
        cbYear = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        tFine = new javax.swing.JTextField();
        pButtons = new javax.swing.JPanel();
        lSearch = new javax.swing.JLabel();
        tSearch = new javax.swing.JTextField();
        bNew = new javax.swing.JButton();
        bSave = new javax.swing.JButton();
        bUpdate = new javax.swing.JButton();
        bDelete = new javax.swing.JButton();
        bClear = new javax.swing.JButton();
        pTable = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        SDetails = new javax.swing.JTable();

        rbTemp.setText("jRadioButton1");

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(700, 453));

        pOnStudents.setBackground(new java.awt.Color(255, 255, 255));
        pOnStudents.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Students", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        pDetails.setBackground(new java.awt.Color(255, 255, 255));
        pDetails.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        jLabel1.setText("Name");

        tRollNo.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N
        tRollNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tRollNoActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        jLabel2.setText("Student ID");

        tSID.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N
        tSID.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tSIDFocusGained(evt);
            }
        });
        tSID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tSIDActionPerformed(evt);
            }
        });
        tSID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tSIDKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tSIDKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        jLabel3.setText("Roll Number");

        tName.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        jLabel4.setText("Gender");

        rbMale.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N
        rbMale.setText("Male");
        rbMale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbMaleActionPerformed(evt);
            }
        });

        rbFemale.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N
        rbFemale.setText("Female");
        rbFemale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbFemaleActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        jLabel5.setText("Department");

        jLabel6.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        jLabel6.setText("Year");

        cbDept.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N
        cbDept.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Computer Science", "Electronics", "Mechanical", "Civil", "Others" }));

        cbYear.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N
        cbYear.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1", "2", "3", "4" }));

        jLabel7.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        jLabel7.setText("Fine");

        tFine.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N

        javax.swing.GroupLayout pDetailsLayout = new javax.swing.GroupLayout(pDetails);
        pDetails.setLayout(pDetailsLayout);
        pDetailsLayout.setHorizontalGroup(
            pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pDetailsLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7)
                    .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pDetailsLayout.createSequentialGroup()
                            .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel2))
                            .addGap(33, 33, 33)
                            .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(tName, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(tSID, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(pDetailsLayout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(18, 18, 18)
                            .addComponent(tRollNo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pDetailsLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(tFine, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pDetailsLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pDetailsLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbYear, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pDetailsLayout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(rbMale)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbFemale))
                            .addGroup(pDetailsLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(cbDept, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(48, 48, 48))))
        );
        pDetailsLayout.setVerticalGroup(
            pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pDetailsLayout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tSID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(rbMale)
                    .addComponent(rbFemale))
                .addGap(18, 18, 18)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(tName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(cbDept, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(tRollNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)))
                .addGap(18, 18, 18)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(tFine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pButtons.setBackground(new java.awt.Color(255, 255, 255));
        pButtons.setBorder(new javax.swing.border.MatteBorder(null));

        lSearch.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        lSearch.setText("Search");

        tSearch.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        bNew.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bNew.setText("New");
        bNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNewActionPerformed(evt);
            }
        });

        bSave.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bSave.setText("Save");
        bSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSaveActionPerformed(evt);
            }
        });

        bUpdate.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bUpdate.setText("Update");
        bUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bUpdateActionPerformed(evt);
            }
        });

        bDelete.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bDelete.setText("Delete");
        bDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteActionPerformed(evt);
            }
        });

        bClear.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bClear.setText("Clear");
        bClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pButtonsLayout = new javax.swing.GroupLayout(pButtons);
        pButtons.setLayout(pButtonsLayout);
        pButtonsLayout.setHorizontalGroup(
            pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pButtonsLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(lSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(bNew)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bUpdate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bClear)
                .addGap(0, 9, Short.MAX_VALUE))
        );
        pButtonsLayout.setVerticalGroup(
            pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lSearch)
                    .addComponent(tSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNew)
                    .addComponent(bSave)
                    .addComponent(bUpdate)
                    .addComponent(bDelete)
                    .addComponent(bClear))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pTable.setBackground(new java.awt.Color(255, 255, 255));
        pTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        SDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Name", "Roll Number", "Gender", "Department", "Year", "Fine"
            }
        ));
        SDetails.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SDetailsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(SDetails);

        javax.swing.GroupLayout pTableLayout = new javax.swing.GroupLayout(pTable);
        pTable.setLayout(pTableLayout);
        pTableLayout.setHorizontalGroup(
            pTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        pTableLayout.setVerticalGroup(
            pTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout pOnStudentsLayout = new javax.swing.GroupLayout(pOnStudents);
        pOnStudents.setLayout(pOnStudentsLayout);
        pOnStudentsLayout.setHorizontalGroup(
            pOnStudentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pButtons, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pOnStudentsLayout.setVerticalGroup(
            pOnStudentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pOnStudentsLayout.createSequentialGroup()
                .addComponent(pDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pButtons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pOnStudents, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pOnStudents, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tRollNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tRollNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tRollNoActionPerformed

    private void rbMaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbMaleActionPerformed
        // TODO add your handling code here:
        gender = "Male";
    }//GEN-LAST:event_rbMaleActionPerformed

    private void rbFemaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbFemaleActionPerformed
        // TODO add your handling code here:
        gender = "Female";
    }//GEN-LAST:event_rbFemaleActionPerformed

    private void bSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSaveActionPerformed
        // TODO add your handling code here:
        if(operation == INSERT){
            insertRecord();
        }else if(operation == UPDATE){
            updateRecord();
        }
        operation = NOP;
        bClear.setEnabled(false);
        bDelete.setEnabled(false);
        bNew.setEnabled(true);
        bSave.setEnabled(false);
        bUpdate.setEnabled(false);
    }//GEN-LAST:event_bSaveActionPerformed

    private void SDetailsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SDetailsMouseClicked
        // TODO add your handling code here:
        int selectedRow = SDetails.getSelectedRow();
        String selectedSID = SDetails.getModel().getValueAt(selectedRow, 0).toString();
        try{
            String sql = "SELECT * FROM students WHERE student_id = '" + selectedSID + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            if(rs.next()){
                tSID.setText(rs.getString("student_id"));
                tName.setText(rs.getString("student_name"));
                tRollNo.setText(rs.getString("student_rno"));
                
                String genderSelected = rs.getString("student_gender");
                if("Male".equals(genderSelected)){
                    gender = "Male";
                    rbMale.setSelected(true);
                }else if("Female".equals(genderSelected)){
                    gender = "Female";
                    rbFemale.setSelected(true);
                }
    
                cbDept.setSelectedItem(rs.getString("student_department"));
                cbYear.setSelectedItem(rs.getString("student_year"));
                tFine.setText(rs.getString("student_fine"));
              
                disableAll();
                bUpdate.setEnabled(true);
                bDelete.setEnabled(true);
                bNew.setEnabled(false);
                bSave.setEnabled(false);
                bClear.setEnabled(false);              
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error : " + e);
        }
    }//GEN-LAST:event_SDetailsMouseClicked

    private void bClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bClearActionPerformed
        // TODO add your handling code here:
        clearFields();
    }//GEN-LAST:event_bClearActionPerformed

    private void bDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteActionPerformed
        // TODO add your handling code here:
        int ans = JOptionPane.showConfirmDialog(this, "Are you sure you want to delete this record?", "Delete!", JOptionPane.YES_NO_OPTION);
        if(ans == JOptionPane.YES_OPTION){
            String sql = "DELETE FROM students WHERE student_id = ?";
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, tSID.getText());
                ps.execute();
                sql = "SELECT * FROM students";
                updateTableData(sql);
                clearFields();
                bNew.setEnabled(true);
                bDelete.setEnabled(false);
                bClear.setEnabled(false);
                bUpdate.setEnabled(false);
            }catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error While Deleting this record!");
            }
        }else if(ans == JOptionPane.NO_OPTION){

        }
    }//GEN-LAST:event_bDeleteActionPerformed

    private void bUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bUpdateActionPerformed
        // TODO add your handling code here:
        operation = UPDATE;
        enableAll();
        bNew.setEnabled(false);
        bUpdate.setEnabled(false);
        bSave.setEnabled(true);
        bClear.setEnabled(false);
        bDelete.setEnabled(false);
    }//GEN-LAST:event_bUpdateActionPerformed

    private void bNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNewActionPerformed
        // TODO add your handling code here:
        clearFields();
        enableAll();
        bNew.setEnabled(false);
        bSave.setEnabled(true);
        bClear.setEnabled(true);
        operation = INSERT;
    }//GEN-LAST:event_bNewActionPerformed

    private void tSIDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tSIDFocusGained

    }//GEN-LAST:event_tSIDFocusGained

    private void tSIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tSIDActionPerformed

    }//GEN-LAST:event_tSIDActionPerformed

    private void tSIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tSIDKeyTyped
    
    }//GEN-LAST:event_tSIDKeyTyped

    private void tSIDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tSIDKeyReleased
        // TODO add your handling code here:
        if(Pattern.compile("n.+").matcher(tSID.getText()).matches()){
            tRollNo.setEnabled(false);
            cbYear.setEnabled(false);
            cbYear.setSelectedIndex(0);
        }else{
            tRollNo.setEnabled(true);
            cbYear.setEnabled(true);
        }
    }//GEN-LAST:event_tSIDKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable SDetails;
    private javax.swing.JButton bClear;
    private javax.swing.JButton bDelete;
    private javax.swing.JButton bNew;
    private javax.swing.JButton bSave;
    private javax.swing.JButton bUpdate;
    private javax.swing.JComboBox<String> cbDept;
    private javax.swing.JComboBox<String> cbYear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lSearch;
    private javax.swing.JPanel pButtons;
    private javax.swing.JPanel pDetails;
    private javax.swing.JPanel pOnStudents;
    private javax.swing.JPanel pTable;
    private javax.swing.JRadioButton rbFemale;
    private javax.swing.JRadioButton rbMale;
    private javax.swing.JRadioButton rbTemp;
    private javax.swing.JTextField tFine;
    private javax.swing.JTextField tName;
    private javax.swing.JTextField tRollNo;
    private javax.swing.JTextField tSID;
    private javax.swing.JTextField tSearch;
    // End of variables declaration//GEN-END:variables
}
