
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class BookIssue extends javax.swing.JPanel {

    /**
     * Creates new form BookIssue
     */
    BookIssue bi;
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    public static final int INSERT = 1;
    int operation = 1;
    public BookIssue() {
        initComponents();
        this.bi = bi;
        conn = MysqlConnect.connectDB();
        fillComboSID();
        fillComboBID();
    }
    
    private void clearFields(){
        cbSID.setSelectedItem(-1);
        cbBID.setSelectedItem(-1);
        dcDOI.setDate(null);
        dcDOR.setDate(null);
    }
    
    private void updateQuantity(){
        if(!"".equals(cbBID.getSelectedItem().toString())){
            String sql = "update books set book_quantity = book_quantity - 1 where book_id = ?";
            String bq = cbBID.getSelectedItem().toString();
            try{
                ps = conn.prepareStatement(sql);
                ps.setString(1, bq);
                ps.execute();
//                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
        }
    }
    private void checkFine(){
        String studentID = cbSID.getSelectedItem().toString();
            try{
                String sql = "select student_fine from students where student_id = '" + studentID + "'";
                ps = conn.prepareStatement(sql);
                rs = ps.executeQuery();
                while(rs.next()){
                    String fine = rs.getString("student_fine");
//                    System.out.println(fine);   
                    if(Integer.parseInt(fine)>500){
                         JOptionPane.showMessageDialog(this,"Don't issue any book, this student's fine is more than Rs500");
                         clearFields();
                    }else{
                    }
                }
            }catch(Exception e){
               JOptionPane.showMessageDialog(this, "Error! " + e); 
               e.printStackTrace();
            }
    }
    
    private void fillComboSID(){
        try{
            String sql = "select * from students";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
//                System.out.println(rs.getString("publication_id"));
                String sid = rs.getString("student_id");
                cbSID.addItem(sid);
            }
            cbSID.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    checkFine();
                }
            });
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    
    private void fillComboBID(){
        try{
            String sql = "select * from books";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
//                System.out.println(rs.getString("publication_id"));
                String bid = rs.getString("book_id");
                cbBID.addItem(bid);
            }
            cbBID.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    checkBookQuantity();
                }
            });
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    
    private void checkBookQuantity(){
        String bid = cbBID.getSelectedItem().toString();
        String sql1 = "select book_quantity from books where book_id = '" + bid + "'";
        try{
            ps = conn.prepareStatement(sql1);
            rs = ps.executeQuery();
            while(rs.next()){
                String bq = rs.getString("book_quantity");              
                if(!(Integer.parseInt(bq)>0)){
                    JOptionPane.showMessageDialog(this, "No books left, Please order some books!");
                    operation = 2;
                }else{
                    operation = 1;
                }
            }
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(this, "Error : " + ex.getMessage());
        }
    }
    
    private void insertRecord(){
        String sql = "INSERT INTO bookissue(student_id,book_id,date_of_issue,date_of_return) VALUES (?, ?, ?, ?)";
        String sid = cbSID.getSelectedItem().toString();
        String bid = cbBID.getSelectedItem().toString();

        Date selectedDate = dcDOI.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDateIssue = sdf.format(selectedDate);

        Date selectedDate1 = dcDOR.getDate();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDateReturn = sdf1.format(selectedDate1);

        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, sid);
            ps.setString(2, bid);
            ps.setString(3, formattedDateIssue);
            ps.setString(4, formattedDateReturn);


            ps.execute();
            JOptionPane.showMessageDialog(this, "Book Issued Successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pBookIssue = new javax.swing.JPanel();
        lSID = new javax.swing.JLabel();
        lBID = new javax.swing.JLabel();
        lDOI = new javax.swing.JLabel();
        lDOR = new javax.swing.JLabel();
        bSubmit = new javax.swing.JButton();
        lNote = new javax.swing.JLabel();
        dcDOR = new com.toedter.calendar.JDateChooser();
        dcDOI = new com.toedter.calendar.JDateChooser();
        cbSID = new javax.swing.JComboBox<>();
        cbBID = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(255, 255, 255));

        pBookIssue.setBackground(new java.awt.Color(255, 255, 255));
        pBookIssue.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Book Issue", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        lSID.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lSID.setText("Student ID");

        lBID.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lBID.setText("Book ID");

        lDOI.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lDOI.setText("Date Of Issue");

        lDOR.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lDOR.setText("Date Of Return");

        bSubmit.setFont(new java.awt.Font("Georgia", 1, 24)); // NOI18N
        bSubmit.setText("Submit");
        bSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSubmitActionPerformed(evt);
            }
        });

        lNote.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lNote.setText("Note : Student whose fine is more than Rs500 is not allowed to issue a book,Sorry!");

        cbSID.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        cbSID.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbSIDItemStateChanged(evt);
            }
        });

        cbBID.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        cbBID.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbBIDItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout pBookIssueLayout = new javax.swing.GroupLayout(pBookIssue);
        pBookIssue.setLayout(pBookIssueLayout);
        pBookIssueLayout.setHorizontalGroup(
            pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBookIssueLayout.createSequentialGroup()
                .addGroup(pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pBookIssueLayout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addGroup(pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lSID)
                            .addComponent(lBID))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbSID, 0, 140, Short.MAX_VALUE)
                            .addComponent(cbBID, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(53, 53, 53)
                        .addGroup(pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lDOR)
                            .addComponent(lDOI))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dcDOR, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                            .addComponent(dcDOI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(pBookIssueLayout.createSequentialGroup()
                        .addGap(259, 259, 259)
                        .addComponent(bSubmit)))
                .addGap(0, 42, Short.MAX_VALUE))
            .addGroup(pBookIssueLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(lNote)
                .addContainerGap(34, Short.MAX_VALUE))
        );
        pBookIssueLayout.setVerticalGroup(
            pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pBookIssueLayout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lDOI)
                        .addComponent(lSID)
                        .addComponent(cbSID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(dcDOI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                .addGroup(pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pBookIssueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lBID)
                        .addComponent(lDOR)
                        .addComponent(cbBID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(dcDOR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(71, 71, 71)
                .addComponent(bSubmit)
                .addGap(57, 57, 57)
                .addComponent(lNote)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBookIssue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBookIssue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSubmitActionPerformed
        // TODO add your handling code here:
        if(operation == 1){
            insertRecord();
            clearFields();
            updateQuantity();
       }
    }//GEN-LAST:event_bSubmitActionPerformed

    private void cbSIDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbSIDItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbSIDItemStateChanged

    private void cbBIDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbBIDItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbBIDItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bSubmit;
    private javax.swing.JComboBox<String> cbBID;
    private javax.swing.JComboBox<String> cbSID;
    private com.toedter.calendar.JDateChooser dcDOI;
    private com.toedter.calendar.JDateChooser dcDOR;
    private javax.swing.JLabel lBID;
    private javax.swing.JLabel lDOI;
    private javax.swing.JLabel lDOR;
    private javax.swing.JLabel lNote;
    private javax.swing.JLabel lSID;
    private javax.swing.JPanel pBookIssue;
    // End of variables declaration//GEN-END:variables
}
