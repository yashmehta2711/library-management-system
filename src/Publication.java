
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class Publication extends javax.swing.JPanel {

    /**
     * Creates new form Publication
     */
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public static final int NOP = 0;
    public static final int INSERT = 1;
    public static final int UPDATE = 2;
    int operation;
    Publication pb;
    public Publication() {
        initComponents();
        this.pb = this;
        conn = MysqlConnect.connectDB();
        String sql = "SELECT * FROM publication";
        updateTableData(sql);
        disableAll();
        tSearch.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
    }
    
    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"ID","Name","Contact Number","Email"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    private void enableAll(){
        tID.setEnabled(true);
        tName.setEnabled(true);
        tNo.setEnabled(true);
        tEmail.setEnabled(true);
    }
    
    private void disableAll(){
        tID.setEnabled(false);
        tName.setEnabled(false);
        tNo.setEnabled(false);
        tEmail.setEnabled(false);
    }
    
    private void searchFilter(){
        String key = tSearch.getText();
        String sql;
        if(key.equals("")){
            sql = "SELECT * FROM publication";
        }else{
            sql = "SELECT * FROM publication WHERE publication_name like '%"+key+"%' OR publication_id like '%"+key+"%' OR publication_number like '%"+key+"%' OR publication_email like '%"+key+"%'";
        }
        updateTableData(sql);
    }
    
    private void insertRecord(){
        String sql = "INSERT INTO publication(publication_id,publication_name,publication_number,publication_email) VALUES (?, ?, ?, ?)";
        String id = tID.getText();
        String name = tName.getText();
        String cno = tNo.getText();
        String email = tEmail.getText();

        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, name);
            ps.setString(3, cno);
            ps.setString(4, email);
            
            ps.execute();
            clearFields();
            String loadTableQuery = "SELECT * FROM publication";
            updateTableData(loadTableQuery);
            JOptionPane.showMessageDialog(this, "Record Inserted Successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
        }
    }
    
    private void updateRecord(){
        if(!"".equals(tID.getText())){
            String sql = "UPDATE publication SET publication_id = ?, publication_name = ?, publication_number = ?, publication_email = ? WHERE publication_id = ?";

            String id = tID.getText();
            String name = tName.getText();
            String cno = tNo.getText();
            String email = tEmail.getText();
               
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, id);
                ps.setString(2, name);
                ps.setString(3, cno);
                ps.setString(4, email);
                ps.setString(5, id);

                ps.execute();
                clearFields();
                String loadTableQuery = "SELECT * FROM publication";
                updateTableData(loadTableQuery);
                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
        }
    }
    
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tbDisplay.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }finally{
            try{
                rs.close();
                ps.close();
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
            }
        }
    }
    
    private void clearFields(){
        tID.setText("");
        tName.setText("");
        tNo.setText("");
        tEmail.setText("");
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pOnPublication = new javax.swing.JPanel();
        pDetails = new javax.swing.JPanel();
        lID = new javax.swing.JLabel();
        lName = new javax.swing.JLabel();
        lNo = new javax.swing.JLabel();
        lEmail = new javax.swing.JLabel();
        tName = new javax.swing.JTextField();
        tID = new javax.swing.JTextField();
        tNo = new javax.swing.JTextField();
        tEmail = new javax.swing.JTextField();
        pButtons = new javax.swing.JPanel();
        lSearch = new javax.swing.JLabel();
        tSearch = new javax.swing.JTextField();
        bNew = new javax.swing.JButton();
        bSave = new javax.swing.JButton();
        bUpdate = new javax.swing.JButton();
        bDelete = new javax.swing.JButton();
        bClear = new javax.swing.JButton();
        pTable = new javax.swing.JPanel();
        spTable = new javax.swing.JScrollPane();
        tbDisplay = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));

        pOnPublication.setBackground(new java.awt.Color(255, 255, 255));
        pOnPublication.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Publications", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        pDetails.setBackground(new java.awt.Color(255, 255, 255));
        pDetails.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lID.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lID.setText("ID");

        lName.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lName.setText("Name");

        lNo.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lNo.setText("Contact Number");

        lEmail.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lEmail.setText("Email");

        tName.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        tName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tNameActionPerformed(evt);
            }
        });

        tID.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        tNo.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        tEmail.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        javax.swing.GroupLayout pDetailsLayout = new javax.swing.GroupLayout(pDetails);
        pDetails.setLayout(pDetailsLayout);
        pDetailsLayout.setHorizontalGroup(
            pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pDetailsLayout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lName)
                    .addComponent(lID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tName)
                    .addComponent(tID, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lNo)
                    .addComponent(lEmail))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tNo, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                    .addComponent(tEmail))
                .addGap(73, 73, 73))
        );
        pDetailsLayout.setVerticalGroup(
            pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pDetailsLayout.createSequentialGroup()
                .addGap(96, 96, 96)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lName)
                    .addComponent(lEmail)
                    .addComponent(tName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pDetailsLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lID)
                    .addComponent(lNo)
                    .addComponent(tID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(109, 109, 109))
        );

        pButtons.setBackground(new java.awt.Color(255, 255, 255));
        pButtons.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lSearch.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lSearch.setText("Search");

        tSearch.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        bNew.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        bNew.setText("New");
        bNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNewActionPerformed(evt);
            }
        });

        bSave.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        bSave.setText("Save");
        bSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSaveActionPerformed(evt);
            }
        });

        bUpdate.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        bUpdate.setText("Update");
        bUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bUpdateActionPerformed(evt);
            }
        });

        bDelete.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        bDelete.setText("Delete");
        bDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteActionPerformed(evt);
            }
        });

        bClear.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        bClear.setText("Clear");
        bClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pButtonsLayout = new javax.swing.GroupLayout(pButtons);
        pButtons.setLayout(pButtonsLayout);
        pButtonsLayout.setHorizontalGroup(
            pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pButtonsLayout.createSequentialGroup()
                .addComponent(lSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bNew)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bUpdate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bClear))
        );
        pButtonsLayout.setVerticalGroup(
            pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lSearch)
                    .addComponent(tSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNew)
                    .addComponent(bSave)
                    .addComponent(bUpdate)
                    .addComponent(bDelete)
                    .addComponent(bClear))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pTable.setBackground(new java.awt.Color(255, 255, 255));
        pTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tbDisplay.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Name", "Contact Number", "Email"
            }
        ));
        tbDisplay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbDisplayMouseClicked(evt);
            }
        });
        spTable.setViewportView(tbDisplay);

        javax.swing.GroupLayout pTableLayout = new javax.swing.GroupLayout(pTable);
        pTable.setLayout(pTableLayout);
        pTableLayout.setHorizontalGroup(
            pTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spTable)
                .addContainerGap())
        );
        pTableLayout.setVerticalGroup(
            pTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spTable, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout pOnPublicationLayout = new javax.swing.GroupLayout(pOnPublication);
        pOnPublication.setLayout(pOnPublicationLayout);
        pOnPublicationLayout.setHorizontalGroup(
            pOnPublicationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pButtons, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pOnPublicationLayout.setVerticalGroup(
            pOnPublicationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pOnPublicationLayout.createSequentialGroup()
                .addComponent(pDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pButtons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pOnPublication, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pOnPublication, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tNameActionPerformed

    private void bSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSaveActionPerformed
        // TODO add your handling code here:
        if(operation == INSERT){
            insertRecord();
        }else if(operation == UPDATE){
            updateRecord();
        }
        operation = NOP;
        bClear.setEnabled(false);
        bDelete.setEnabled(false);
        bNew.setEnabled(true);
        bSave.setEnabled(false);
        bUpdate.setEnabled(false);
    }//GEN-LAST:event_bSaveActionPerformed

    private void tbDisplayMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbDisplayMouseClicked
        // TODO add your handling code here:
        int selectedRow = tbDisplay.getSelectedRow();
        String selectedSID = tbDisplay.getModel().getValueAt(selectedRow, 0).toString();
        try{
            String sql = "SELECT * FROM publication WHERE publication_id = '" + selectedSID + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            if(rs.next()){
                tID.setText(rs.getString("publication_id"));
                tName.setText(rs.getString("publication_name"));
                tNo.setText(rs.getString("publication_number"));
                tEmail.setText(rs.getString("publication_email"));
        
                disableAll();
                bUpdate.setEnabled(true);
                bDelete.setEnabled(true);
                bNew.setEnabled(false);
                bSave.setEnabled(false);
                bClear.setEnabled(false);
                
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error : " + e);
        }
    }//GEN-LAST:event_tbDisplayMouseClicked

    private void bClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bClearActionPerformed
        // TODO add your handling code here:
        clearFields();
    }//GEN-LAST:event_bClearActionPerformed

    private void bDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteActionPerformed
        // TODO add your handling code here:
        int ans = JOptionPane.showConfirmDialog(this, "Are you sure you want to delete this record?", "Delete!", JOptionPane.YES_NO_OPTION);
        if(ans == JOptionPane.YES_OPTION){
            //You have pressed yes, so deleting the selected record!
            String sql = "DELETE FROM publication WHERE publication_id = ?";
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, tID.getText());
                ps.execute();
                sql = "SELECT * FROM publication";
                updateTableData(sql);
                clearFields();
                bNew.setEnabled(true);
                bDelete.setEnabled(false);
                bClear.setEnabled(false);
                bUpdate.setEnabled(false);
            }catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error While Deleting this record!");
            }
        }else if(ans == JOptionPane.NO_OPTION){
            //You have pressed no, so NOP is performed
        }
    }//GEN-LAST:event_bDeleteActionPerformed

    private void bUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bUpdateActionPerformed
        // TODO add your handling code here:
        operation = UPDATE;
        enableAll();
        bNew.setEnabled(false);
        bUpdate.setEnabled(false);
        bSave.setEnabled(true);
        bClear.setEnabled(false);
        bDelete.setEnabled(false);
    }//GEN-LAST:event_bUpdateActionPerformed

    private void bNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNewActionPerformed
        // TODO add your handling code here:
        clearFields();
        enableAll();
        bNew.setEnabled(false);
        bSave.setEnabled(true);
        bClear.setEnabled(true);
        operation = INSERT;
    }//GEN-LAST:event_bNewActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bClear;
    private javax.swing.JButton bDelete;
    private javax.swing.JButton bNew;
    private javax.swing.JButton bSave;
    private javax.swing.JButton bUpdate;
    private javax.swing.JLabel lEmail;
    private javax.swing.JLabel lID;
    private javax.swing.JLabel lName;
    private javax.swing.JLabel lNo;
    private javax.swing.JLabel lSearch;
    private javax.swing.JPanel pButtons;
    private javax.swing.JPanel pDetails;
    private javax.swing.JPanel pOnPublication;
    private javax.swing.JPanel pTable;
    private javax.swing.JScrollPane spTable;
    private javax.swing.JTextField tEmail;
    private javax.swing.JTextField tID;
    private javax.swing.JTextField tName;
    private javax.swing.JTextField tNo;
    private javax.swing.JTextField tSearch;
    private javax.swing.JTable tbDisplay;
    // End of variables declaration//GEN-END:variables
}
