

import java.awt.Dimension;
import java.awt.Toolkit;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class HomePage extends javax.swing.JFrame {

    /**
     * Creates new form HomePage
     */
    
    HomePage hp;
    public HomePage() {
        initComponents();
        this.hp = this;
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dimension.width / 2 - this.getWidth() / 2, dimension.height / 2 - this.getHeight() / 2);
        
        Dashboard dashboard = new Dashboard();
        dashboard.setBounds(0, 0, 681, 439);
        pCR.add(dashboard);
        dashboard.setVisible(true);
        pack();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pHome = new javax.swing.JPanel();
        pUpper = new javax.swing.JPanel();
        lImg = new javax.swing.JLabel();
        bLogout = new javax.swing.JButton();
        lLMS = new javax.swing.JLabel();
        pServices = new javax.swing.JPanel();
        pLeft = new javax.swing.JPanel();
        pButtons = new javax.swing.JPanel();
        bHome = new javax.swing.JButton();
        bStudents = new javax.swing.JButton();
        bBooks = new javax.swing.JButton();
        bRequest = new javax.swing.JButton();
        bIB = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        pCR = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        pHome.setBackground(new java.awt.Color(255, 255, 255));

        pUpper.setBackground(new java.awt.Color(255, 255, 255));

        lImg.setBackground(new java.awt.Color(255, 255, 255));
        lImg.setForeground(new java.awt.Color(255, 255, 255));
        lImg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/download.jpg"))); // NOI18N

        bLogout.setFont(new java.awt.Font("Georgia", 1, 24)); // NOI18N
        bLogout.setText("Logout");
        bLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bLogoutActionPerformed(evt);
            }
        });

        lLMS.setFont(new java.awt.Font("Georgia", 1, 36)); // NOI18N
        lLMS.setText("Library Management System");

        javax.swing.GroupLayout pUpperLayout = new javax.swing.GroupLayout(pUpper);
        pUpper.setLayout(pUpperLayout);
        pUpperLayout.setHorizontalGroup(
            pUpperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pUpperLayout.createSequentialGroup()
                .addComponent(lImg, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lLMS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 153, Short.MAX_VALUE)
                .addComponent(bLogout)
                .addContainerGap())
        );
        pUpperLayout.setVerticalGroup(
            pUpperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pUpperLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pUpperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lLMS)
                    .addComponent(bLogout))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pServices.setBackground(new java.awt.Color(255, 255, 255));

        pLeft.setBackground(new java.awt.Color(255, 255, 255));

        pButtons.setBackground(new java.awt.Color(255, 255, 255));
        pButtons.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Dasboard", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 18))); // NOI18N

        bHome.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        bHome.setText("Home");
        bHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bHomeActionPerformed(evt);
            }
        });

        bStudents.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        bStudents.setText("Students");
        bStudents.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bStudentsActionPerformed(evt);
            }
        });

        bBooks.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        bBooks.setText("Manage");
        bBooks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bBooksActionPerformed(evt);
            }
        });

        bRequest.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        bRequest.setText("Request");
        bRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bRequestActionPerformed(evt);
            }
        });

        bIB.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        bIB.setText("Issued Books");
        bIB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bIBActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        jButton2.setText("Requested Books");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        jButton1.setText("Publications");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        jButton3.setText("Return");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        jButton4.setText("Issue");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Georgia", 1, 22)); // NOI18N
        jLabel1.setText("Books");

        javax.swing.GroupLayout pButtonsLayout = new javax.swing.GroupLayout(pButtons);
        pButtons.setLayout(pButtonsLayout);
        pButtonsLayout.setHorizontalGroup(
            pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pButtonsLayout.createSequentialGroup()
                .addGroup(pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pButtonsLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jButton2))
                    .addGroup(pButtonsLayout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(bHome))
                    .addGroup(pButtonsLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(bStudents))
                    .addGroup(pButtonsLayout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bRequest)
                            .addComponent(jButton3)
                            .addComponent(bBooks)
                            .addGroup(pButtonsLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jButton4))))
                    .addGroup(pButtonsLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(bIB)
                            .addComponent(jButton1)))
                    .addGroup(pButtonsLayout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pButtonsLayout.setVerticalGroup(
            pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bHome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bStudents)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(bBooks)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bRequest)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bIB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout pLeftLayout = new javax.swing.GroupLayout(pLeft);
        pLeft.setLayout(pLeftLayout);
        pLeftLayout.setHorizontalGroup(
            pLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pButtons, javax.swing.GroupLayout.PREFERRED_SIZE, 176, Short.MAX_VALUE)
                .addContainerGap())
        );
        pLeftLayout.setVerticalGroup(
            pLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pButtons, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pCR.setBackground(new java.awt.Color(255, 255, 255));
        pCR.setPreferredSize(new java.awt.Dimension(681, 439));

        javax.swing.GroupLayout pCRLayout = new javax.swing.GroupLayout(pCR);
        pCR.setLayout(pCRLayout);
        pCRLayout.setHorizontalGroup(
            pCRLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pCRLayout.setVerticalGroup(
            pCRLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pServicesLayout = new javax.swing.GroupLayout(pServices);
        pServices.setLayout(pServicesLayout);
        pServicesLayout.setHorizontalGroup(
            pServicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pServicesLayout.createSequentialGroup()
                .addComponent(pLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pCR, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pServicesLayout.setVerticalGroup(
            pServicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pServicesLayout.createSequentialGroup()
                .addGroup(pServicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pLeft, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pCR, javax.swing.GroupLayout.DEFAULT_SIZE, 440, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout pHomeLayout = new javax.swing.GroupLayout(pHome);
        pHome.setLayout(pHomeLayout);
        pHomeLayout.setHorizontalGroup(
            pHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pUpper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pServices, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pHomeLayout.setVerticalGroup(
            pHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pHomeLayout.createSequentialGroup()
                .addComponent(pUpper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pServices, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bLogoutActionPerformed
        // TODO add your handling code here:
        new Login().setVisible(true);
        hp.dispose();
    }//GEN-LAST:event_bLogoutActionPerformed

    
    private void bStudentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bStudentsActionPerformed
        // TODO add your handling code here:
        pCR.removeAll();
        Student student = new Student();
        student.setBounds(0, 0, pCR.getWidth(), pCR.getHeight());
        pCR.add(student);
        student.setVisible(true);
        pack();
    }//GEN-LAST:event_bStudentsActionPerformed

    private void bRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bRequestActionPerformed
        // TODO add your handling code here:
        pCR.removeAll();        
        BookRequest br = new BookRequest();
        br.setBounds(0, 0, 681, 439);
        pCR.add(br);
        br.setVisible(true);
        pack();
    }//GEN-LAST:event_bRequestActionPerformed

    private void bBooksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bBooksActionPerformed
        // TODO add your handling code here:
        pCR.removeAll();        
        Book book = new Book();
        book.setBounds(0, 0, 681, 439);
        pCR.add(book);
        book.setVisible(true);
        pack();

        
    }//GEN-LAST:event_bBooksActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        pCR.removeAll();        
        Publication pb = new Publication();
        pb.setBounds(0, 0, 681, 439);
        pCR.add(pb);
        pb.setVisible(true);
        pack();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void bHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bHomeActionPerformed
        // TODO add your handling code here:
        pCR.removeAll();
        Dashboard dashboard = new Dashboard();
        dashboard.setBounds(0, 0, 681, 439);
        pCR.add(dashboard);
        dashboard.setVisible(true);
        pack();
    }//GEN-LAST:event_bHomeActionPerformed

    private void bIBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bIBActionPerformed
        // TODO add your handling code here:
        pCR.removeAll();      
        BookIssued bi = new BookIssued();
        bi.setBounds(0, 0, 681, 439);
        pCR.add(bi);
        bi.setVisible(true);
        pack();
    }//GEN-LAST:event_bIBActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        pCR.removeAll();      
        BookRequested br = new BookRequested();
        br.setBounds(0, 0, 681, 439);
        pCR.add(br);
        br.setVisible(true);
        pack();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        pCR.removeAll();     
        BookIssue bi = new BookIssue();
        bi.setBounds(0, 0, 681, 439);
        pCR.add(bi);
        bi.setVisible(true);
        pack();
//        this.updateUI();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        pCR.removeAll();     
        BookReturn br = new BookReturn();
        br.setBounds(0, 0, 681, 439);
        pCR.add(br);
        br.setVisible(true);
        pack();
//        this.updateUI();
    }//GEN-LAST:event_jButton3ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new HomePage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bBooks;
    private javax.swing.JButton bHome;
    private javax.swing.JButton bIB;
    private javax.swing.JButton bLogout;
    private javax.swing.JButton bRequest;
    private javax.swing.JButton bStudents;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lImg;
    private javax.swing.JLabel lLMS;
    private javax.swing.JPanel pButtons;
    private javax.swing.JPanel pCR;
    private javax.swing.JPanel pHome;
    private javax.swing.JPanel pLeft;
    private javax.swing.JPanel pServices;
    private javax.swing.JPanel pUpper;
    // End of variables declaration//GEN-END:variables
}
