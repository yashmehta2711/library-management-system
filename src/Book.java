
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class Book extends javax.swing.JPanel {

    /**
     * Creates new form Book
     */
    Book book;
    BookRequest br;
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public static final int NOP = 0;
    public static final int INSERT = 1;
    public static final int UPDATE = 2;
    int operation;
    
    public Book() {
        initComponents();
        this.book = this;
        conn = MysqlConnect.connectDB();
        fillComboPName();
        conn = MysqlConnect.connectDB();
        String sql = "SELECT * FROM books";
        updateTableData(sql);
        disableAll();
        tSearch.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
    }
    
    private void fillComboPName(){
        try{
            String sql = "select * from publication";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
//                System.out.println(rs.getString("book_id"));
                String pName = rs.getString("publication_name");
                cbPName.addItem(pName);
            }
            
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
        }
    }
    
    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"ID","Name","Author","Quantity"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    private void enableAll(){
        tID.setEnabled(true);
        tName.setEnabled(true);
        tAuthor.setEnabled(true);
        tQuantity.setEnabled(true);
        cbPName.setEnabled(true);
    }
    
    private void disableAll(){
        tID.setEnabled(false);
        tName.setEnabled(false);
        tAuthor.setEnabled(false);
        tQuantity.setEnabled(false);
        cbPName.setEnabled(false);
    }
    
    private void searchFilter(){
        String key = tSearch.getText();
        String sql;
        if(key.equals("")){
            sql = "SELECT * FROM books";
        }else{
            sql = "SELECT * FROM books WHERE book_name like '%"+key+"%' OR book_id like '%"+key+"%' OR book_author like '%"+key+"%' OR book_quantity like '%"+key+"%' OR publication_name like '%"+key+"%'";
        }
        updateTableData(sql);
    }
    
    private void insertRecord(){
        String sql = "INSERT INTO books(book_id,book_name,book_author,book_quantity,publication_name) VALUES (?, ?, ?, ?, ?)";
        String id = tID.getText();
        String name = tName.getText();
        String author = tAuthor.getText();
        String quantity = tQuantity.getText();
        String pName = cbPName.getSelectedItem().toString();

        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, name);
            ps.setString(3, author);
            ps.setString(4, quantity);
            ps.setString(5, pName);
   
            ps.execute();
            clearFields();
            String loadTableQuery = "SELECT * FROM books";
            updateTableData(loadTableQuery);
            JOptionPane.showMessageDialog(this, "Record Inserted Successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
        }
    }
    
    private void updateRecord(){
        if(!"".equals(tID.getText())){
            String sql = "Update books set book_id = ?, book_name = ?, book_author = ?, book_quantity = ?, publication_name = ? where book_id = ?";
            String id = tID.getText();
            String name = tName.getText();
            String author = tAuthor.getText();
            String quantity = tQuantity.getText();
            String pName = cbPName.getSelectedItem().toString();
               
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, id);
                ps.setString(2, name);
                ps.setString(3, author);
                ps.setString(4, quantity);
                ps.setString(5, pName);
                ps.setString(6, id);

                ps.execute();
                clearFields();
                String loadTableQuery = "SELECT * FROM books";
                updateTableData(loadTableQuery);
                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
       }
    }
    
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tblBooks.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }finally{
            try{
                rs.close();
                ps.close();
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
            }
        }
    }
    
    private void clearFields(){
        tID.setText("");
        tName.setText("");
        tAuthor.setText("");
        tQuantity.setText("");
        cbPName.setSelectedIndex(-1);
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pOnBooks = new javax.swing.JPanel();
        pDetails = new javax.swing.JPanel();
        lID = new javax.swing.JLabel();
        lName = new javax.swing.JLabel();
        lAuthor = new javax.swing.JLabel();
        lQuantity = new javax.swing.JLabel();
        lPID = new javax.swing.JLabel();
        tName = new javax.swing.JTextField();
        tID = new javax.swing.JTextField();
        tQuantity = new javax.swing.JTextField();
        tAuthor = new javax.swing.JTextField();
        cbPName = new javax.swing.JComboBox<>();
        pButtons = new javax.swing.JPanel();
        lSearch = new javax.swing.JLabel();
        tSearch = new javax.swing.JTextField();
        bNew = new javax.swing.JButton();
        bSave = new javax.swing.JButton();
        bUpdate = new javax.swing.JButton();
        bDelete = new javax.swing.JButton();
        bClear = new javax.swing.JButton();
        pTable = new javax.swing.JPanel();
        spBooks = new javax.swing.JScrollPane();
        tblBooks = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));

        pOnBooks.setBackground(new java.awt.Color(255, 255, 255));
        pOnBooks.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Books", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        pDetails.setBackground(new java.awt.Color(255, 255, 255));
        pDetails.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lID.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lID.setText("ID");

        lName.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lName.setText("Name");

        lAuthor.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lAuthor.setText("Author");

        lQuantity.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lQuantity.setText("Quantity");

        lPID.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lPID.setText("Publication Name");

        tName.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N

        tID.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N

        tQuantity.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N

        tAuthor.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N

        cbPName.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N

        javax.swing.GroupLayout pDetailsLayout = new javax.swing.GroupLayout(pDetails);
        pDetails.setLayout(pDetailsLayout);
        pDetailsLayout.setHorizontalGroup(
            pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pDetailsLayout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lName)
                    .addComponent(lID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tName, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                    .addComponent(tID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pDetailsLayout.createSequentialGroup()
                        .addComponent(lQuantity)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tQuantity))
                    .addGroup(pDetailsLayout.createSequentialGroup()
                        .addComponent(lAuthor)
                        .addGap(18, 18, 18)
                        .addComponent(tAuthor, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)))
                .addGap(77, 77, 77))
            .addGroup(pDetailsLayout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addComponent(lPID)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbPName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(222, 222, 222))
        );
        pDetailsLayout.setVerticalGroup(
            pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pDetailsLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lID)
                    .addComponent(lAuthor)
                    .addComponent(tID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tAuthor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lName)
                    .addComponent(lQuantity)
                    .addComponent(tName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(pDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lPID)
                    .addComponent(cbPName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        pButtons.setBackground(new java.awt.Color(255, 255, 255));
        pButtons.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lSearch.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        lSearch.setText("Search");

        tSearch.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        bNew.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bNew.setText("New");
        bNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bNewActionPerformed(evt);
            }
        });

        bSave.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bSave.setText("Save");
        bSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSaveActionPerformed(evt);
            }
        });

        bUpdate.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bUpdate.setText("Update");
        bUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bUpdateActionPerformed(evt);
            }
        });

        bDelete.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bDelete.setText("Delete");
        bDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteActionPerformed(evt);
            }
        });

        bClear.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        bClear.setText("Clear");
        bClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pButtonsLayout = new javax.swing.GroupLayout(pButtons);
        pButtons.setLayout(pButtonsLayout);
        pButtonsLayout.setHorizontalGroup(
            pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(bNew)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bUpdate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bClear)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        pButtonsLayout.setVerticalGroup(
            pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lSearch)
                    .addComponent(tSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bNew)
                    .addComponent(bSave)
                    .addComponent(bUpdate)
                    .addComponent(bDelete)
                    .addComponent(bClear))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pTable.setBackground(new java.awt.Color(255, 255, 255));
        pTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tblBooks.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "ID", "Name", "Author", "Quantity", "P Name"
            }
        ));
        tblBooks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblBooksMouseClicked(evt);
            }
        });
        spBooks.setViewportView(tblBooks);

        javax.swing.GroupLayout pTableLayout = new javax.swing.GroupLayout(pTable);
        pTable.setLayout(pTableLayout);
        pTableLayout.setHorizontalGroup(
            pTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spBooks)
        );
        pTableLayout.setVerticalGroup(
            pTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spBooks, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pOnBooksLayout = new javax.swing.GroupLayout(pOnBooks);
        pOnBooks.setLayout(pOnBooksLayout);
        pOnBooksLayout.setHorizontalGroup(
            pOnBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pButtons, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pOnBooksLayout.setVerticalGroup(
            pOnBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pOnBooksLayout.createSequentialGroup()
                .addComponent(pDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pButtons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pOnBooks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pOnBooks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSaveActionPerformed
        // TODO add your handling code here:
        if(operation == INSERT){
            insertRecord();
        }else if(operation == UPDATE){
            updateRecord();
        }
        operation = NOP;
        bClear.setEnabled(false);
        bDelete.setEnabled(false);
        bNew.setEnabled(true);
        bSave.setEnabled(false);
        bUpdate.setEnabled(false);
    }//GEN-LAST:event_bSaveActionPerformed

    private void tblBooksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblBooksMouseClicked
        // TODO add your handling code here:
        int selectedRow = tblBooks.getSelectedRow();
        String selectedSID = tblBooks.getModel().getValueAt(selectedRow, 0).toString();
        try{
            String sql = "SELECT * FROM books WHERE book_id = '" + selectedSID + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            if(rs.next()){
                tID.setText(rs.getString("book_id"));
                tName.setText(rs.getString("book_name"));
                tAuthor.setText(rs.getString("book_author"));
                tQuantity.setText(rs.getString("book_quantity"));
                cbPName.setSelectedItem(rs.getString("publication_name"));
        
                disableAll();
                bUpdate.setEnabled(true);
                bDelete.setEnabled(true);
                bNew.setEnabled(false);
                bSave.setEnabled(false);
                bClear.setEnabled(false);
                
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error : " + e);
        }
    }//GEN-LAST:event_tblBooksMouseClicked

    private void bClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bClearActionPerformed
        // TODO add your handling code here:
        clearFields();
    }//GEN-LAST:event_bClearActionPerformed

    private void bDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteActionPerformed
        // TODO add your handling code here:
        int ans = JOptionPane.showConfirmDialog(this, "Are you sure you want to delete this record?", "Delete!", JOptionPane.YES_NO_OPTION);
        if(ans == JOptionPane.YES_OPTION){
            //You have pressed yes, so deleting the selected record!
            String sql = "DELETE FROM books WHERE book_id = ?";
            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, tID.getText());
                ps.execute();
                sql = "SELECT * FROM books";
                updateTableData(sql);
                clearFields();
                bNew.setEnabled(true);
                bDelete.setEnabled(false);
                bClear.setEnabled(false);
                bUpdate.setEnabled(false);
            }catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error While Deleting this record!");
            }
        }else if(ans == JOptionPane.NO_OPTION){
            //You have pressed no, so NOP is performed
        }
    }//GEN-LAST:event_bDeleteActionPerformed

    private void bUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bUpdateActionPerformed
        // TODO add your handling code here:
        operation = UPDATE;
        enableAll();
        bNew.setEnabled(false);
        bUpdate.setEnabled(false);
        bSave.setEnabled(true);
        bClear.setEnabled(false);
        bDelete.setEnabled(false);
    }//GEN-LAST:event_bUpdateActionPerformed

    private void bNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bNewActionPerformed
        // TODO add your handling code here:
        clearFields();
        enableAll();
        bNew.setEnabled(false);
        bSave.setEnabled(true);
        bClear.setEnabled(true);
        operation = INSERT;
    }//GEN-LAST:event_bNewActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bClear;
    private javax.swing.JButton bDelete;
    private javax.swing.JButton bNew;
    private javax.swing.JButton bSave;
    private javax.swing.JButton bUpdate;
    private javax.swing.JComboBox<String> cbPName;
    private javax.swing.JLabel lAuthor;
    private javax.swing.JLabel lID;
    private javax.swing.JLabel lName;
    private javax.swing.JLabel lPID;
    private javax.swing.JLabel lQuantity;
    private javax.swing.JLabel lSearch;
    private javax.swing.JPanel pButtons;
    private javax.swing.JPanel pDetails;
    private javax.swing.JPanel pOnBooks;
    private javax.swing.JPanel pTable;
    private javax.swing.JScrollPane spBooks;
    private javax.swing.JTextField tAuthor;
    private javax.swing.JTextField tID;
    private javax.swing.JTextField tName;
    private javax.swing.JTextField tQuantity;
    private javax.swing.JTextField tSearch;
    private javax.swing.JTable tblBooks;
    // End of variables declaration//GEN-END:variables
}
