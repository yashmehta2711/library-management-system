
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
/*
api key
D25YKN76J6RDN72JEPP2J1RH2JANL699
secret key
YE9PC3R0VH25Y9HV
*/
public class BookRequest extends javax.swing.JPanel {

    /**
     * Creates new form BookRequest
     */
    ArrayList<String> pNumber = new ArrayList<String>();
    BookRequest br;
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
   
        
    static String url = "https://www.sms4india.com";

    public BookRequest() {
        initComponents();
        this.br = this;
        conn = MysqlConnect.connectDB();
        fillComboPName();
        fillComboBName();
    }
    
    public static String sendCampaign(String apiKey,String secretKey,String useType, String phone, String message, String senderId){
      try{
          // construct data
        JSONObject urlParameters = new JSONObject();
        urlParameters.put("apikey",apiKey);
        urlParameters.put("secret",secretKey);
        urlParameters.put("usetype",useType);
        urlParameters.put("phone", phone);
        urlParameters.put("message", URLEncoder.encode(message,"UTF-8"));
        urlParameters.put("senderid", senderId);
        URL obj = new URL(url + "/api/v1/sendCampaign");
          // send data
        HttpURLConnection httpConnection = (HttpURLConnection) obj.openConnection();
        httpConnection.setDoOutput(true);
        httpConnection.setRequestMethod("POST");
        DataOutputStream wr = new DataOutputStream(httpConnection.getOutputStream());
        wr.write(urlParameters.toString().getBytes());
        // get the response  
        BufferedReader bufferedReader = null;
        if (httpConnection.getResponseCode() == 200) {
            bufferedReader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
        } else {
            bufferedReader = new BufferedReader(new InputStreamReader(httpConnection.getErrorStream()));
        }
        StringBuilder content = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            content.append(line).append("\n");
        }
        bufferedReader.close();
        return content.toString();
      }catch(Exception ex){
        System.out.println("Exception at:" + ex);
        return "{'status':500,'message':'Internal Server Error'}";
      }
        
    }
    
    private void clearFields(){
        cbBName.setSelectedItem(-1);
        cbPName.setSelectedItem(-1);
        tQuantity.setText("");
        tPNo.setText("");
    }
     
    private void fillPNo(){
        String pname = cbPName.getSelectedItem().toString();
        try{
            String sql = "select * from publication where publication_name = '" + pname + "';";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
//                System.out.println(rs.getString("publication_id"));
                String pno = rs.getString("publication_number");
//                System.out.println(pno);               
                tPNo.setText(pno);
            }
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    private void fillComboPName(){
        try{
            String sql = "select * from publication";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
//                System.out.println(rs.getString("publication_id"));
                String pid = rs.getString("publication_name");
                cbPName.addItem(pid);
            }
            cbPName.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    fillPNo();
                }
            });
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    
    private void fillComboBName(){
        try{
            String sql = "select * from books";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
//                System.out.println(rs.getString("book_id"));
                String bid = rs.getString("book_name");
                cbBName.addItem(bid);
            }
            
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
        }
    }
    
    private void insertRecord(){
        String sql = "INSERT INTO bookrequest(book_name,publication_name,quantity,publication_number) VALUES (?, ?, ?, ?)";
        String bname = cbBName.getSelectedItem().toString();
        String pname = cbPName.getSelectedItem().toString();
        String q = tQuantity.getText();
        String pno = tPNo.getText();
 
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, bname);
            ps.setString(2, pname);
            ps.setString(3, q);
            ps.setString(4, pno);
      
            ps.execute();
            JOptionPane.showMessageDialog(this, "Request Sent Successfully!");
        }catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pBookRequest = new javax.swing.JPanel();
        lBID = new javax.swing.JLabel();
        lQuantity = new javax.swing.JLabel();
        lPID = new javax.swing.JLabel();
        lPNo = new javax.swing.JLabel();
        tQuantity = new javax.swing.JTextField();
        tPNo = new javax.swing.JTextField();
        bSubmit = new javax.swing.JButton();
        lNote = new javax.swing.JLabel();
        cbPName = new javax.swing.JComboBox<>();
        cbBName = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(255, 255, 255));

        pBookRequest.setBackground(new java.awt.Color(255, 255, 255));
        pBookRequest.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Book Request", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        lBID.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lBID.setText("Book Name");

        lQuantity.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lQuantity.setText("Quantity");

        lPID.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lPID.setText("Publication Name");

        lPNo.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lPNo.setText("Publication Number");

        tQuantity.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        tPNo.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        bSubmit.setFont(new java.awt.Font("Georgia", 1, 24)); // NOI18N
        bSubmit.setText("Submit");
        bSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSubmitActionPerformed(evt);
            }
        });

        lNote.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lNote.setText("Note : Message will be sent to the number provided above.");

        cbPName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbPNameItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout pBookRequestLayout = new javax.swing.GroupLayout(pBookRequest);
        pBookRequest.setLayout(pBookRequestLayout);
        pBookRequestLayout.setHorizontalGroup(
            pBookRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBookRequestLayout.createSequentialGroup()
                .addGroup(pBookRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pBookRequestLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(pBookRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lBID)
                            .addComponent(lQuantity))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pBookRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbBName, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36)
                        .addGroup(pBookRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pBookRequestLayout.createSequentialGroup()
                                .addComponent(lPNo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tPNo, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pBookRequestLayout.createSequentialGroup()
                                .addComponent(lPID)
                                .addGap(28, 28, 28)
                                .addComponent(cbPName, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(pBookRequestLayout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addComponent(lNote))
                    .addGroup(pBookRequestLayout.createSequentialGroup()
                        .addGap(252, 252, 252)
                        .addComponent(bSubmit)))
                .addContainerGap(29, Short.MAX_VALUE))
        );
        pBookRequestLayout.setVerticalGroup(
            pBookRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBookRequestLayout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addGroup(pBookRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lBID)
                    .addComponent(lPID)
                    .addComponent(cbPName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbBName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                .addGroup(pBookRequestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lQuantity)
                    .addComponent(tQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lPNo)
                    .addComponent(tPNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49)
                .addComponent(bSubmit)
                .addGap(73, 73, 73)
                .addComponent(lNote)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBookRequest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBookRequest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSubmitActionPerformed
        // TODO add your handling code here:
        insertRecord();
        String pno = tPNo.getText();
//        System.out.println(pno);
        String q = tQuantity.getText();
//        System.out.println(q);
        String msg = "Hello! Yash Mehta's library need " + cbBName.getSelectedItem().toString() + "'s " + q + " Books. Thankyou!";
//        System.out.println(msg);
        sendCampaign("D25YKN76J6RDN72JEPP2J1RH2JANL699","YE9PC3R0VH25Y9HV","stage",pno,msg,"SMSIND");
        clearFields();
    }//GEN-LAST:event_bSubmitActionPerformed

    private void cbPNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbPNameItemStateChanged
        // TODO add your handling code here:       
    }//GEN-LAST:event_cbPNameItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bSubmit;
    private javax.swing.JComboBox<String> cbBName;
    private javax.swing.JComboBox<String> cbPName;
    private javax.swing.JLabel lBID;
    private javax.swing.JLabel lNote;
    private javax.swing.JLabel lPID;
    private javax.swing.JLabel lPNo;
    private javax.swing.JLabel lQuantity;
    private javax.swing.JPanel pBookRequest;
    private javax.swing.JTextField tPNo;
    private javax.swing.JTextField tQuantity;
    // End of variables declaration//GEN-END:variables
}
