
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class MysqlConnect {
    Connection conn;
    public static Connection connectDB(){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "yash","yash");
//            JOptionPane.showMessageDialog(null, "Connection Established Successfully!");
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Connection Failed! " + e);
            return null;
        }
    }
}


