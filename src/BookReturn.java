
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class BookReturn extends javax.swing.JPanel {

    /**
     * Creates new form BookReturn
     */
    ArrayList<String> studentID = new ArrayList<String>();
    BookReturn br;
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    public static final int NOP = 0;
    public static final int PROPER = 1;
    public static final int IMPROPER = 2;
    int operation;
    String condition = "";
    public BookReturn() {
        initComponents();
        this.br = br;
        conn = MysqlConnect.connectDB();
        fillComboSID();            
    }
    
    private void clearFields(){
        cbSID.setSelectedItem(-1);
        cbBID.setSelectedItem(-1);
        tFine.setText("");
        rbTemp.setSelected(true);
        rbProper.setSelected(false);
        rbImproper.setSelected(false);
        condition = null;
    }
    
    private void fillComboSID(){
        try{
            String sql = "select * from bookissue";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            String sid = "";
            while(rs.next()){
                sid = rs.getString("student_id");
                studentID.add(sid);
            }
//            cbSID.addItem("none");
            for(String stid : studentID){
                cbSID.addItem(stid);
            }
            cbSID.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    String sid = cbSID.getSelectedItem().toString();
                    fillComboBID(sid);
                }
            });
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    
    private void fillComboBID(String studentID){
        try{
            String sql = "select * from bookissue where student_id = '" + studentID + "';";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            cbBID.removeAllItems();
            while(rs.next()){
//                System.out.println(rs.getString("publication_id"));
                String bid = rs.getString("book_id");
                cbBID.addItem(bid);
            }
            
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    
    private void insertRecord(boolean proper){
        if((!(tFine.getText()).equals("0"))){
            String sql = "INSERT INTO bookreturn(student_id,book_id,book_condition,fine) VALUES (?, ?, ?, ?)";
            String sid = cbSID.getSelectedItem().toString();
            String bid = cbBID.getSelectedItem().toString();
            int fine = Integer.parseInt(tFine.getText());

            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, sid);
                ps.setString(2, bid);
                ps.setString(3, condition);
                ps.setInt(4, fine);
                ps.execute();
                
                if(proper == false){
                    updateFine(fine);
                }
                JOptionPane.showMessageDialog(this, "Book Returned Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
            }
        }else{
            String sql = "INSERT INTO bookreturn(student_id,book_id,book_condition) VALUES (?, ?, ?)";
            String sid = cbSID.getSelectedItem().toString();
            String bid = cbBID.getSelectedItem().toString();

            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, sid);
                ps.setString(2, bid);
                ps.setString(3, condition);
                ps.execute();
                
                JOptionPane.showMessageDialog(this, "Book Returned Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
            }
        }
    }
    private void updateAddQuantity(){
        if(!"".equals(cbBID.getSelectedItem().toString())){
            String sql = "update books set book_quantity = book_quantity + 1 where book_id = ?";
            String bq = cbBID.getSelectedItem().toString();
            try{
                ps = conn.prepareStatement(sql);
                ps.setString(1, bq);
                ps.execute();
//                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
        }
    }
    
    private void updateMinusQuantity(){
        if(!"".equals(cbBID.getSelectedItem().toString())){
            String sql = "update books set book_quantity = book_quantity - 1 where book_id = ?";
            String bq = cbBID.getSelectedItem().toString();
            try{
                ps = conn.prepareStatement(sql);
                ps.setString(1, bq);
                ps.execute();
//                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
        }
    }
    
    private void updateFine(int fine){
        String id = cbSID.getSelectedItem().toString();
        if(fine!=0){
            String sql = "select student_fine from students where student_id = ?";
            try{
                ps = conn.prepareStatement(sql);
                ps.setString(1, id);
                ps.executeQuery();
                int student_fine = 0;
                while(rs.next()){
                    student_fine = rs.getInt(1);
                }
                student_fine += fine;
                
                sql = "update students set student_fine = " + student_fine + " where student_id = ?";
                try{
                    ps = conn.prepareStatement(sql);
                    ps.setString(1, id);
                    ps.execute();
                    JOptionPane.showMessageDialog(this, "Fine Assigned Successfully!");
                }catch(SQLException ex){
                    JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
                }
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            } 
        }
    }
    
    private void deleteFromIssue(){
        String sql = "DELETE FROM bookissue WHERE student_id = ?";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, cbSID.getSelectedItem().toString());
            ps.execute();
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(this, "Error While Deleting this record!");
        }     
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbTemp = new javax.swing.JRadioButton();
        pBookReturn = new javax.swing.JPanel();
        lSID = new javax.swing.JLabel();
        lBID = new javax.swing.JLabel();
        lCondition = new javax.swing.JLabel();
        lFine = new javax.swing.JLabel();
        rbProper = new javax.swing.JRadioButton();
        rbImproper = new javax.swing.JRadioButton();
        tFine = new javax.swing.JTextField();
        bSubmit = new javax.swing.JButton();
        lNote = new javax.swing.JLabel();
        cbSID = new javax.swing.JComboBox<>();
        cbBID = new javax.swing.JComboBox<>();

        rbTemp.setText("jRadioButton1");

        setBackground(new java.awt.Color(255, 255, 255));

        pBookReturn.setBackground(new java.awt.Color(255, 255, 255));
        pBookReturn.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Book Return", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        lSID.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lSID.setText("Student ID");

        lBID.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lBID.setText("Book ID");

        lCondition.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lCondition.setText("Condition");

        lFine.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lFine.setText("Fine");

        rbProper.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        rbProper.setText("Proper");
        rbProper.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rbProperItemStateChanged(evt);
            }
        });
        rbProper.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbProperActionPerformed(evt);
            }
        });
        rbProper.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rbProperKeyReleased(evt);
            }
        });

        rbImproper.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        rbImproper.setText("Improper");
        rbImproper.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rbImproperItemStateChanged(evt);
            }
        });
        rbImproper.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbImproperActionPerformed(evt);
            }
        });

        tFine.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        bSubmit.setFont(new java.awt.Font("Georgia", 1, 24)); // NOI18N
        bSubmit.setText("Submit");
        bSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSubmitActionPerformed(evt);
            }
        });

        lNote.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lNote.setText("Note : Fine must be applied to student if book returned is improper.");

        cbSID.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbSIDItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout pBookReturnLayout = new javax.swing.GroupLayout(pBookReturn);
        pBookReturn.setLayout(pBookReturnLayout);
        pBookReturnLayout.setHorizontalGroup(
            pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBookReturnLayout.createSequentialGroup()
                .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pBookReturnLayout.createSequentialGroup()
                        .addGap(63, 63, 63)
                        .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pBookReturnLayout.createSequentialGroup()
                                .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lSID)
                                    .addComponent(lBID))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbSID, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbBID, 0, 140, Short.MAX_VALUE))
                                .addGap(47, 47, 47)
                                .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lCondition)
                                    .addComponent(lFine)))
                            .addGroup(pBookReturnLayout.createSequentialGroup()
                                .addComponent(bSubmit)
                                .addGap(51, 51, 51)))
                        .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pBookReturnLayout.createSequentialGroup()
                                .addComponent(rbProper)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbImproper))
                            .addComponent(tFine, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pBookReturnLayout.createSequentialGroup()
                        .addGap(80, 80, 80)
                        .addComponent(lNote)))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        pBookReturnLayout.setVerticalGroup(
            pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pBookReturnLayout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lSID)
                        .addComponent(cbSID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lCondition)
                        .addComponent(rbProper)
                        .addComponent(rbImproper)))
                .addGap(98, 98, 98)
                .addGroup(pBookReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lBID)
                    .addComponent(lFine)
                    .addComponent(tFine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbBID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(54, 54, 54)
                .addComponent(bSubmit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addComponent(lNote)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBookReturn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pBookReturn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void rbProperActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbProperActionPerformed
        // TODO add your handling code here:
        condition = "Proper";
    }//GEN-LAST:event_rbProperActionPerformed

    private void rbImproperActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbImproperActionPerformed
        // TODO add your handling code here:
        condition = "Improper";
    }//GEN-LAST:event_rbImproperActionPerformed

    private void bSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSubmitActionPerformed
        // TODO add your handling code here:
        if(operation == 1){
            insertRecord(true);
            clearFields();
            deleteFromIssue();
            updateAddQuantity();
        }else if(operation == 2){
            insertRecord(false);
            clearFields();
            updateMinusQuantity();
            deleteFromIssue();
        }
        operation = NOP;
    }//GEN-LAST:event_bSubmitActionPerformed

    private void rbProperKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rbProperKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_rbProperKeyReleased

    private void rbProperItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbProperItemStateChanged
        // TODO add your handling code here:
        operation = 1;
        tFine.setEnabled(false);
        tFine.setText("0");
    }//GEN-LAST:event_rbProperItemStateChanged

    private void rbImproperItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbImproperItemStateChanged
        // TODO add your handling code here:
        operation = 2;
        tFine.setEnabled(true);
    }//GEN-LAST:event_rbImproperItemStateChanged

    private void cbSIDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbSIDItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbSIDItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bSubmit;
    private javax.swing.JComboBox<String> cbBID;
    private javax.swing.JComboBox<String> cbSID;
    private javax.swing.JLabel lBID;
    private javax.swing.JLabel lCondition;
    private javax.swing.JLabel lFine;
    private javax.swing.JLabel lNote;
    private javax.swing.JLabel lSID;
    private javax.swing.JPanel pBookReturn;
    private javax.swing.JRadioButton rbImproper;
    private javax.swing.JRadioButton rbProper;
    private javax.swing.JRadioButton rbTemp;
    private javax.swing.JTextField tFine;
    // End of variables declaration//GEN-END:variables
}
